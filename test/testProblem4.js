// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const inventory = require('../data');

const dealersCarsYear = require('../problem4');

const yearList = dealersCarsYear(inventory) ;

if (yearList.length != 0) {
    console.log("All the years from every car on the lot", yearList);
} else {
    console.log("please inventory data is not in proper format ");
}