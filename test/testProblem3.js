// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const inventory = require('../data');

const getCarModelListAlphabetically = require('../problem3');

const carModelList = getCarModelListAlphabetically(inventory);

if (carModelList.length != 0) {
    console.log("car model list alphabetically ", carModelList);
} else {
    console.log("please inventory data is not in proper format ");
}