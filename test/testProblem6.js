// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const inventory = require('../data');

const BMWAndAudiCars = require('../problem6');

const carIdJsonList = BMWAndAudiCars(inventory);

if (carIdJsonList.length != 0) {
    console.log("BMW and Audi cars", carIdJsonList);
} else {
    console.log("please inventory data is not in proper format ");
}