// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


const inventory = require('../data');

const getOlderCars = require('../problem5');

const carCount = getOlderCars(inventory);

if (carCount != -1) {
    console.log(carCount, "cars are older than the year 2000.")

} else {
    console.log("please inventory data is not in proper format ");
}
