// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
// "Car 33 is a *car year goes here* *car make goes here* *car model goes here*"


function getCarById(inventory , carId){

    // error handling
    if(Array.isArray(inventory) && (carId >= 1 && carId <= 50)){
        let size = inventory.length ;
        for(let index =0 ; index < size ; index++){
            if(inventory[index]["id"] === carId){
                let result = `Car ${inventory[index]['id']} is a ${inventory[index]['car_year']} ${inventory[index]['car_make']} ${inventory[index]['car_model']} ` ;
                return result ;
            }
        }
        // we didn't find any car corresponding to given car_id ;
        return "" ;

    }else{
        return "";
    }

}

module.exports = getCarById ;