// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

function BMWAndAudiCars(inventory) {

    // error handling
    if (Array.isArray(inventory)) {

        let size = inventory.length;

        const carId = []

        for (let index = 0; index < size; index++) {
            if (inventory[index]['car_make'] === 'BMW' || inventory[index]['car_make'] === 'Audi') {
                carId.push(inventory[index]);
            }
        }

        const carIdJsonList = JSON.stringify(carId);
        return carIdJsonList;


    } else {
        return [];
    }

}

module.exports = BMWAndAudiCars;