// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function dealersCarsYear(inventory) {

    // error handling
    if (Array.isArray(inventory)) {
        let size = inventory.length;

        let yearList = []

        for (let index = 0; index < size; index++) {
            yearList.push(inventory[index]['car_year']);
        }
        return yearList;

    } else {
        return [];
    }

}

module.exports = dealersCarsYear;