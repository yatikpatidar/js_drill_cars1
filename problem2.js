
// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
// "Last car is a *car make goes here* *car model goes here*"

function getLastCarDetail(inventory) {

    // error handling
    if (Array.isArray(inventory)) {
        let size = inventory.length;

        if (size === 0) {
            return "we don't have any car in our inventory";
        }
        else {
            let result = `Last car is a ${inventory[size - 1]['car_make']} ${inventory[size - 1]['car_model']}`;
            return result;
        }

    } else {
        return "";
    }

}

module.exports = getLastCarDetail;