// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function getCarModelListAlphabetically(inventory) {

    // error handling
    if (Array.isArray(inventory)) {
        let size = inventory.length;

        let carModelList = []
        for (let index = 0; index < size; index++) {
            carModelList.push(inventory[index]['car_model'])
        }

        return carModelList.sort();

    } else {
        return [];
    }

}

module.exports = getCarModelListAlphabetically;