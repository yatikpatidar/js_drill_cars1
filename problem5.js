// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


function getOlderCars(inventory) {

    // error handling
    if (Array.isArray(inventory)) {

        const dealersCarsYear = require('./problem4');

        const yearList = dealersCarsYear(inventory);

        let size = yearList.length;

        let carCount = 0;
        for (let index = 0; index < size; index++) {
            if (yearList[index] < 2000) {
                carCount += 1;
            }
        }
        return carCount;

    } else {
        return -1 ;
    }

}

module.exports = getOlderCars;